/*
 * $Id$
 *
 * Copyright 2009-2010 Roger H. Joerg. All rights reserved.
 *
 * Unless required by applicable law or agreed to in writing, this software
 * is distributed on an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
 * KIND, either express or implied.
 */
package ch.musterdorf.kino.api.core.model;

import java.util.Date;

/**
 * @author Roger H. Joerg
 */
public interface Show
{
	public Theater getTheater();

	public Movie getMovie();

	public long getStart();

	public Date getStartDate();
}
