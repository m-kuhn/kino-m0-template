/*
 * $Id$
 *
 * Copyright 2009-2010 Roger H. Joerg. All rights reserved.
 *
 * Unless required by applicable law or agreed to in writing, this software
 * is distributed on an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
 * KIND, either express or implied.
 */
package ch.musterdorf.kino.api.core.model;

import java.util.List;

/**
 * @author Roger H. Joerg
 */
public interface Movie
{
	public Long getId();

	public String getTitle();

	public List<Show> getShows();
}
