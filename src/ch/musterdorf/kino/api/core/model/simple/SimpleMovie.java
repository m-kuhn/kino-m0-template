/*
 * $Id$
 *
 * Copyright 2009-2010 Roger H. Joerg. All rights reserved.
 *
 * Unless required by applicable law or agreed to in writing, this software
 * is distributed on an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
 * KIND, either express or implied.
 */
package ch.musterdorf.kino.api.core.model.simple;

import java.util.List;

import ch.musterdorf.kino.api.core.model.Movie;
import ch.musterdorf.kino.api.core.model.Show;

/**
 * @author Roger H. Joerg
 */
public class SimpleMovie implements Movie {
	

	
	private final Long id;

	private final String title;

	// private final List<SimpleShow> shows = new ArrayList<SimpleShow>();

	public SimpleMovie(Long id, String title)
	{
		this.id = id;
		this.title = title;
	}

	@Override
	public Long getId()
	{
		return id;
	}

	@Override
	public String getTitle()
	{
		return title;
	}

	@Override
	public List<Show> getShows()
	{
		throw new UnsupportedOperationException("not yet implemented");
		// return new ArrayList<Show>(shows);
	}
	//
	// protected void add(SimpleShow show)
	// {
	// shows.add(show);
	// }
}
